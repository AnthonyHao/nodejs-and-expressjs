const express = require('express')
const path = require('path')
const app = express()
const port = 4000
const ROOT_DIRECTORY = path.resolve(__dirname);

// Serve static files

//Create virtual path for files.
app.use( '/static', express.static(path.join(ROOT_DIRECTORY, 'public')))
//Use multiple static assets directories.
app.use( '/static', express.static(path.join(ROOT_DIRECTORY, 'public2')))
// 1.   当两个静态文件资料夹共享同一个虚拟路径时，两者出现同名文件的情况下，会返回先定义的资料夹内的文件
//      此处在 /static/testimg.jpg 下显示的图片为 public 中的 testimg.jpg
// 2.   使用 path.join(__dirname, 'public') 的绝对路径防止路径错误

// Handle 404 responses

// Express do not see 404 as a result of error so the error-handler middleware will not capture them.
// Following middleware function is needed to handle 404 response.
app.use((req,res,next)=>{
    res.status(404).send("Can not find anything, sorry about that!")
})

// Setup error handler
const errorHandler = (err,req,res,next)=>{
    res.status(500).send('Server Error!')
}
app.use(errorHandler)

// Basic routing

app.get('/', (req,res) => res.send('Hello World!'))
app.post('/', (req,res) => res.send('Got a POST request'))
app.get('/user', (req,res) => res.send('Got a GET request on /user'))
app.put('/user', (req,res) => res.send('Got a PUT request on /user'))
app.delete('/user', (req,res) => res.send('Got a DELETE request on /user'))


app.listen(port, ()=> console.log(`Example app listening on port ${port}!`))