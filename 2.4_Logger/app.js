const express = require('express')
const path = require('path')
const fs = require('fs')
// morgan is used as logger middleware for expressJs or can be just used for nodeJs
const morgan = require('morgan')
const uuid = require('node-uuid')
const FileStreamRotator = require('file-stream-rotator')

const app = express()
const port = 4000
const ROOT_DIRECTORY = path.resolve(__dirname);


// Basic use of morgan, logs will be shown in terminal:
// app.use(morgan('short'));

// to store logs locally
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'),{flags:'a'})
app.use(morgan('short', {
    stream: accessLogStream,
    skip: (req,res) => (res.statusCode >=400)
}))
// loggers can be use seperately for different purposes
var errorLogStream = fs.createWriteStream(path.join(__dirname, 'error.log'),{flags:'a'})
app.use(morgan('short', {
    stream: errorLogStream,
    skip: (req,res) => (res.statusCode <400)
}))


// morgan format can be customized
// even token can be customized
const assignId = (req,res,next) => {
    req.id = uuid.v4()
    next()
}
app.use(assignId)
morgan.token('from', (req,res)=>(req.query.from || '-'))
morgan.token('id', (req,res)=>(req.id))
morgan.format('test','[test] :method :url :status :id :from')
// loggers can be stored by date to make a clear storage
var accessDailyLogStream = FileStreamRotator.getStream({
    date_format:'YYYYMMDD',
    filename: path.join(path.join(__dirname,'log'),'access-%DATE%.log'),
    frequency: 'daily',
    verbose: false
}) 
app.use(morgan('test', {stream: accessDailyLogStream}))

app.get('/static/:filename',(req,res,next)=>{
    fs.readFile(path.join(ROOT_DIRECTORY,'public',req.params.filename),(err,data)=>{
        if(err){
            next(err)
        }else {
            next()
        }
    })
},(req,res,next)=>{
    res.send('This is a available image!')
},(err,req,res,next)=>{
    res.status(500).send('Server Error!')
})

app.get('*', (req,res,next)=>{
    res.sendFile(path.resolve(ROOT_DIRECTORY,'client','index.html'));
});

app.use((req,res,next)=>{
    res.status(404).send("Can not find anything, sorry about that!")
})

const errorHandler = (err,req,res,next)=>{
    res.status(500).send('Server Error!')
}
app.use(errorHandler)

app.listen(port, ()=> console.log(`Example app listening on port ${port}!`))