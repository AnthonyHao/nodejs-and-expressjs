import axios from 'axios';

const postAPI = axios.create({ baseURL: 'https://backend.lasfu.roro3.com/lasfu-back-end/api' });

export const getPost = async ({ id }) => {
	console.log(id)
    const fetchedPost = await postAPI.get('/selectAPost', { params: { id, language:'en' } }).then(res => ({ item: res.data.post?res.data.post:{} }));
	return fetchedPost;
};