import React,{useMemo} from 'react';
import {withRouter} from 'react-router-dom';

const PostDetailSection = () => {
    return(
        <div>
            detail
        </div>
    )
}

const PostListSection = () => {
    return(
        <div>
            list
        </div>
    )
}

const PostPage = (props) =>{
    console.log('check app, post',props.initState)
    const {location:{pathname}} = props;
    const render = useMemo(()=>{
        if(/\/detail/g.test(pathname)){
            return(
                <PostDetailSection/>
            )
        }else{
            return(
                <PostListSection/>
            )
        }
    },[pathname])
    return(
        <div>
            {render}
        </div>
    )
}
export default withRouter(PostPage);