import express from "express";
import compression from "compression";
import ssr from "./routes/ssr";

import bodyParser from 'body-parser';

const app = express();
app.use(bodyParser.json());
app.use(compression());
app.use(express.static("public"));

app.use("/", ssr);

const port = process.env.PORT || 3000;
app.listen(port, function listenHandler() {
  console.info(`Running on ${port}...`);
});