import express from 'express';
import HomePage from '../components/Page/HomePage/HomePage';
import App from '../app';
import React from 'react';
import { renderToString } from "react-dom/server";
import hbs from 'handlebars';
import axios from 'axios';

const router = express.Router();

// router.get("/",async (req,res) => {
//     res.status(201).send("Hello World");
// });
const postAPI = axios.create({ baseURL: 'https://backend.lasfu.roro3.com/lasfu-back-end/api' });

// router.get('/post', async (req,res) => {
//     const itemsRes = await postAPI.get('/V2/postList',{params:{}})
// })

router.get("/post/detail", async (req, res) => {
    const itemRes = await postAPI.get('/selectAPost', { params: { id:req.query.id, language:'en' } }).then(res => ({ item: res.data.post?res.data.post:{} }));
    const theHtml = `
    <html>
    <head><title>${itemRes.item.title}</title></head>
    <body>
    <h1>My First Server Side Render</h1>
    <div id="reactele">{{{reactele}}}</div>
    <script src="/app.js" charset="utf-8"></script>
    <script src="/vendor.js" charset="utf-8"></script>
    </body>
    </html>
    `;
    
    const hbsTemplate = hbs.compile(theHtml);
    const reactComp = renderToString(<App initState={{item:itemRes.item}}/>);
    const htmlToSend = hbsTemplate({reactele:reactComp});
    res.send(htmlToSend);
});

export default router;