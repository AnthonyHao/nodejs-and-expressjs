import React, {Suspense} from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import HomePage from '../components/Page/HomePage/HomePage';
import PostPage from '../components/Page/PostPage/PostPage';

const Routes = () => {
    return(
        <Suspense fallback={<div>Loading...</div>}>
            <Switch>
                <Redirect exact from='/' to={'/home'}/>
                <Route path={'/post'} component={PostPage}/>
                <Route path={'/home'} component={HomePage}/>
            </Switch>
        </Suspense>
    )
}
export default Routes;