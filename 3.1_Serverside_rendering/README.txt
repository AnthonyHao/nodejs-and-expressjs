1.  Create a directory for your code and from the directory execute
    npm init
2.  To create our server, we are going to use ExpressJs. Therefore we are going to include express into our modules
    npm install express
3.  We will be using ES6, therefore Babel is needed to transpile our code. We need these dependencies for development only and not for production release
    npm install --save-dev @babel/cli @babel/core @babel/node @babel/plugin-proposal-class-properties @babel/plugin-transform-runtime @babel/polyfill @babel/preset-env
4.  Create folder src and create server.js
    mkdir src
    touch ./src/server.js #for terminal
    echo $null >> ./src/server.js #for windows
5.  Create a directory under 'src' and create a new file named ssr.js
    mkdir ./src/routes
    echo $null >> ./src/routes/ssr.js #for windows
6.  Create a folder name it 'components', create a new file at '/component/Page/HomePage' named 'HomePage.js'
7.  
