const express = require('express')
const path = require('path')
const birdsRouter = require('./routers/birds');

const app = express()
const port = 4000
const ROOT_DIRECTORY = path.resolve(__dirname);

// Using an array of callback functions can handle a route
var cb0 = (req, res, next) => {
    console.log('CB0')
    next();
}
var cb1 = (req, res, next) => {
    console.log('CB1')
    next();
}
var cb2 = (req, res, next) => {
    res.send('Hello from CB3')
}
app.get('/arrayOfFunctions',[cb0,cb1,cb2])
// Or it can be written as follow:
app.get('/arrayOfFunctions2',[cb0,cb1],(req,res,next)=>{
    console.log('CB3')
    next();
},(req,res,next)=>{
    cb2(req,res,next)
})


// How to use res.download()
app.get('/static/:filename',  (req,res,next)=>{
    res.download(path.join(ROOT_DIRECTORY, 'public',req.params.filename),`test-${req.params.filename}`,(err)=>{
        if(err){
            console.log('Download Fail')
        } else {
            console.log('Download Complete')
        }
    })
})


// Using app.route() to create chainable route handler
app.route('/chain')
    .get((req,res)=>{
        res.send('Get in chain!')
    })
    .post((req,res)=>{
        res.send('Post in chain')
    })
    .put((req,res)=>{
        res.send('Put in chain')
    })


// Using birds router imported from router folder
app.use('/birds',birdsRouter)    


const errorHandler = (err,req,res,next)=>{
    res.status(500).send('Server Error!')
}
app.use(errorHandler)


app.listen(port, ()=> console.log(`Example app listening on port ${port}!`))