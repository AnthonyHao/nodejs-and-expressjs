1. Use "npm init" to setup node
2. Set entry point to 'app.js'
3. Install express by "npm install express --save"
3. Create app.js under root
4. Try express server with "node app.js"
5. Use "npm init react-app client" to create a react app with Creat React App
6. Move to the newly created folder 'client'
6. Check react app with "yarn start", project should be started at 'http://localhost:3000/'
7. Run "npm install react-router-dom" to install react-router-dom
8. Update 'index.js' to start setting up client-side routing
9. Create '/router/routes.js' to handle routing, 'router' folder will be handling router for react app
10. Create testing pages under 'component/Page' to handle render view, 'component' folder will be handling components for react app
11. Install required packges to client with "npm install "/"yarn add " + package name
12. Build react app with "yarn build"
13. Return to root folder, update 'app.js' to serve the react app
14. "node app.js" and enjoy the app at 'http://localhost:4000/' 