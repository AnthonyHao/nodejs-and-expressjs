const express = require('express')
const path = require('path')

const app = express()
const port = 4000
const ROOT_DIRECTORY = path.resolve(__dirname);

app.use(express.static(path.join(__dirname, 'client','build')));

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
  });

app.use((req,res,next)=>{
    res.status(404).send("Can not find anything, sorry about that!")
})

const errorHandler = (err,req,res,next)=>{
    res.status(500).send('Server Error!')
}
app.use(errorHandler)

app.listen(port, ()=> console.log(`Example app listening on port ${port}!`))