import React,{useState,useCallback} from 'react';

const STATUS = {
    HOVERED:'hovered',
    NORMAL:'normal'
};

const Link = props =>{
    const [state,setState] = useState({class:STATUS.NORMAL});
    const _onMouseEnter = useCallback(()=>{
        setState({class:STATUS.HOVERED});
    },[])
    const _onMouseLeave = useCallback(()=>{
        setState({class:STATUS.NORMAL});
    },[])
    return(
        <a
            className={state.class}
            href={props.page||'#'}
            onMouseEnter={_onMouseEnter}
            onMouseLeave={_onMouseLeave}
        >
            {props.children}
        </a>
    )
}
export default Link