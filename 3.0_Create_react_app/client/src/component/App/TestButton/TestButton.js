import React from 'react';
import {Button} from 'semantic-ui-react';

const TestButton = ({items=[],onClick=()=>{}}) => {
    return(
        <div className={"todo-list"}>
            {items.map((item,index)=>{
                <div key={index}>
                    <span className={"item-text"}>{item}</span>
                    <Button onClick={onClick}>done</Button>
                </div>
            })}
        </div>
    )
}

export default TestButton;