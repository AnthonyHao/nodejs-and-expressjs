import React,{useCallback,useEffect,useState} from 'react';
import styled from 'styled-components';

const StyledPage = styled.div`
    h1{
        font-size:20px;
        color:blue;
        font-weight: 700;
    }
    p{
        font-size:10px;
        color:aquamarine;
    }
`

const HomePage = () => {
    const dbPromise = window.indexedDB.open('test-db1',1);
    useEffect(()=>{
        if(!(window.indexedDB)){
            console.log('This browser doesn\'t support IndexedDB')
        }
        dbPromise.onupgradeneeded = (e)=>{
            console.log('Start to create a new object store')
            const db = e.target.result;
            if(!db.objectStoreNames.contains('person')){
                console.log('Create Store')
                db.createObjectStore('users',{keyPath:'id',autoIncrement:false})
            }
        };
    },[])
    const addUser1ToLocalDB = ()=>{
        const request = indexedDB.open('test-db1',1);
        request.onsuccess = (e) => {
            const db = e.target.result;
            const tx = db.transaction('users','readwrite');
            const store = tx.objectStore('users');
            store.add({'id': 1, 'userName': '李白', 'age': 24, 'level':5})
        }
    }
    const addUser2ToLocalDB = ()=>{
        const request = indexedDB.open('test-db1',1);
        request.onsuccess = (e) => {
            const db = e.target.result;
            const tx = db.transaction('users','readwrite');
            const store = tx.objectStore('users');
            store.add({'id': 2, 'userName': 'LI白', 'age': 21, 'level':3})
        }
    }
    const addUser3ToLocalDB = ()=>{
        const request = indexedDB.open('test-db1',1);
        request.onsuccess = (e) => {
            const db = e.target.result;
            const tx = db.transaction('users','readwrite');
            const store = tx.objectStore('users');
            const x = store.add({'id': 3, 'userName': '李BAI', 'age': 23,'level':3})
            x.onerror = (e) =>{
                console.log(e.target.error.message)
            }
        }  
    }
    const [users,setUsers] = useState([]);
    const getUsers = () => {
        const request = indexedDB.open('test-db1',1);
        request.onsuccess = (e) => {
            const db = e.target.result;
            const tx = db.transaction('users','readonly');
            const store = tx.objectStore('users');
            var req= store.getAll();
            req.onsuccess= e=>{
                setUsers(e.target.result)
            }
        }
    }
    const _onDelete = ({id})=>{
        const request = indexedDB.open('test-db1',1);
        request.onsuccess = (e) => {
            const db = e.target.result;
            const tx = db.transaction('users','readwrite');
            const store = tx.objectStore('users');
            var x = store.delete(id)
            x.onerror = e =>{
                console.log(e.target.error.message)
            } 
        }
    }
    return(
        <StyledPage>
            <h1>Hello,world</h1>
            <p>One small step for coders, one giant step for coder Anthony</p>
            <button onClick={addUser1ToLocalDB}>Click to add user 1</button>
            <button onClick={addUser2ToLocalDB}>Click to add user 2</button>
            <button onClick={addUser3ToLocalDB}>Click to add user 3</button>
            <button onClick={getUsers}>get users</button>
            <table>
                <thead>
                    <tr>
                        <th>index</th>
                        <th>id</th>
                        <th>name</th>
                        <th>age</th>
                        <th>level</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                {users.map((user,index)=>(
                    <tr key={index}>
                        <th>{index}</th>
                        <th>{user.id}</th>
                        <th>{user.userName}</th>
                        <th>{user.age}</th>
                        <th>{user.level}</th>
                        <th><button onClick={()=>_onDelete({id:user.id})}>Delete</button></th>
                    </tr>
                ))}
                </tbody>
            </table>
        </StyledPage>
    )
}

export default HomePage;