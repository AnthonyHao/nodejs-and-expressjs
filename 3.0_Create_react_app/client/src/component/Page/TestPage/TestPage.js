import React from 'react';
import styled from 'styled-components';

const StyledPage = styled.div`
    h1{
        font-size:20px;
        color:darkblue;
        font-weight: 700;
    }
    p{
        font-size:10px;
        color:aquamarine;
    }
`

const TestPage = () => {
    return(
        <StyledPage>
            <h1>Hello,world again</h1>
            <p>One more small step for coders, one more giant step for coder Anthony</p>
        </StyledPage>
    )
}

export default TestPage;