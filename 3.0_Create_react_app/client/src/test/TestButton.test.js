import React from 'react';
import jest from 'jest-mock';
import TestButton from '../component/App/TestButton/TestButton';
import Enzyme,{shallow,render,mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

const props = {
    items:['test1','test2'],
    onClick:jest.fn()
}

const setup = () => {
    const wrapper = shallow(<TestButton {...props}/>)
    return{
        props,
        wrapper
    }
}

const setupByRender = () => {
    const wrapper = render(<TestButton {...props}/>)
    return{
        props,
        wrapper
    }
}

const setupByMount = () => {
    const wrapper = mount(<TestButton {...props}/>)
    return{
        props,
        wrapper
    }
}

it('should has Button',()=>{
    const {wrapper} = setup();
    expect(wrapper.find('Button').length).toBe(2);
})

it('should render 2 item',()=>{
    const {wrapper} = setupByRender();
    expect(wrapper.find('button').length).toBe(2)
})

it('should render item equal',()=>{
    const {wrapper} = setupByMount();
    wrapper.find('.item-text').forEach((node,index)=>{
        expect(node.text()).toBe(wrapper.props().items[index])
    })
})

it('click item to be done', () => {
    const {wrapper} = setupByMount();
    wrapper.find('Button').at(0).simulate('click');
    expect(props.onClick).toBeCalled();
})