
import React, { Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import InitLoad from '../App';

const HomePage = React.lazy(()=> import('../component/Page/HomePage/HomePage'));
const TestPage = React.lazy(()=> import('../component/Page/TestPage/TestPage'));

const Routes = () => {
    return(
		<Suspense fallback={<InitLoad />}>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route exact path='/test' component={TestPage}/>
            </Switch>
        </Suspense>
    )
}

export default Routes; 