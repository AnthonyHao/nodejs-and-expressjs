const express = require('express')
const path = require('path')
const fs = require('fs')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')

const app = express()
const port = 4000
const ROOT_DIRECTORY = path.resolve(__dirname);


// Catch synchronous error requires no extra work. If synchronous code throws error, Express will catch and process it.
app.get('/throwError',(req,res)=>{
    throw new Error('BROKEN')
})


// Asynchronous errors must be passed to the next() function. 
// If you pass anything to the next() function other than 'route' or 'router', Express will catch and process them as error and skip all remaining non-error-handler middleware functions.
app.get('/static/:filename',(req,res,next)=>{
    fs.readFile(path.join(ROOT_DIRECTORY,'public',req.params.filename),(err,data)=>{
        if(err){
            next(err)
        }else {
            next()
        }
    })
},(req,res,next)=>{
    console.log('Passed')
    res.send('This is a available image!')
},(err,req,res,next)=>{
    console.log('Ops, we met an error! ', err)
    res.end()
})


// Use error handle middlewares after other app.use() and Routes calls
const logErrors = (err,req,res,next)=>{
    console.log(err.stack)
    next(err)
}
// when not calling next(), you are responsible for writing the response otherwise those requests will hang and not be eligible for garbage collection
const clientErrorHandler = (err,req,res,next) => {
    if(req.xhr){
        res.status(500).send({error: 'Something failed!'})
    } else {
        next(err)
    }
}
app.use(bodyParser.urlencoded({
    extended: true
})) 
app.use(bodyParser.json())
app.use(methodOverride())
app.use(logErrors)
app.use(clientErrorHandler)
app.use((err,req,res,next)=>{
    console.log('Ops, we met an error at end! ', err)
    res.end()
})

app.listen(port, ()=> console.log(`Example app listening on port ${port}!`))