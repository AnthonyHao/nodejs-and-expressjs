const express = require('express')
const path = require('path')
const router = express.Router()

const app = express()
const port = 4000
const ROOT_DIRECTORY = path.resolve(__dirname);


// Creating simple middleware which log simple text
const simpleLogger = (req,res,next) => {
    console.log('LOGGED')
    next()
}


// Creating configurable middleware
const configuredLogger = (text) =>{
    return (req,res,next) =>{
        console.log(`Configured to ${text}`)
        req.configuredText = text
        next()
    }
}


// Passing values/properties by middleware
const requestTime = (req,res,next) => {
    req.requestTime = Date.now()
    req.text = 'Changed'
    next()
}


// Getting values between middlewares
const propertyCheck = (req,res,next) => {
    console.log('text',req.text)
    next()
}
// middlewares 按顺序处理，因此后处理的middleware可以获取和修改前面定义的property
// 此处 req.text 的值最终为 'Changed'


// Applying defined middlewares
app.use(simpleLogger)
app.use(configuredLogger('test middleware'))
app.use(requestTime)
app.use(propertyCheck)


// ============================ App-level middleware ============================
// Using next('route') to pass control to the next route
app.get('/user/:id', function (req, res, next) {
    // if the user ID is 0, skip to the next route
    if (req.params.id === '0') next('route')
    // otherwise pass the control to the next middleware function in this stack
    else next()
  }, function (req, res, next) {
    // send a regular response
    res.send('regular')
  })
  
  // handler for the /user/:id path, which sends a special response
  app.get('/user/:id', function (req, res, next) {
    res.send('special')
  })


  app.get('/', (req,res,next)=>{
    res.send(`This is a GET request. Requested at ${req.requestTime}`);
});


// ============================ Router-level middleware ============================
// Using next('router') to pass controll to the next router
router.use('/:type/:id',[simpleLogger], (req,res,next)=>{
    if(req.params.id==='0'){
        console.log('special')
        next('router')
    } else {
        next()
    }
},[requestTime,propertyCheck])
router.get('/:type/:id',(req, res, next)=>{
    res.send(`This is a ${req.params.type}, id is ${req.params.id}`)
})
app.use('/cars',router)


// ============================ Error-handling middleware ============================
// Error-level middleware takes four arguments, even if you do not need the next object
const errorHandler = (err,req,res,next)=>{
    res.status(500).send('Server Error!')
}


// ============================ Built-in middleware ============================
// express.static serves static assets like html files and images.
// express.json parses incoming requests with JSON payloads.
// express.urlencoded parses incoming requests with URL-encoded payloads.


// ============================ Third-party middleware ============================
// Install the Node.js module, load it at app-level or router-level.
// First run 'npm install cookie-parser'

// var cookieParser = require('cookie-parser')
// app.use(cookieParser())


app.listen(port, ()=> console.log(`Example app listening on port ${port}!`))