const express = require('express')
const path = require('path')
const app = express()
const port = 4000
const ROOT_DIRECTORY = path.resolve(__dirname);

// express 按行处理middleware, 此处get请求后因 res.sendFile() 终止 request-response cycle 所以不会返回 404 error.
app.get('*', (req,res,next)=>{
    res.sendFile(path.resolve(ROOT_DIRECTORY,'client','index.html'));
});

app.use((req,res,next)=>{
    res.status(404).send("Can not find anything, sorry about that!")
})

const errorHandler = (err,req,res,next)=>{
    res.status(500).send('Server Error!')
}
app.use(errorHandler)

app.listen(port, ()=> console.log(`Example app listening on port ${port}!`))